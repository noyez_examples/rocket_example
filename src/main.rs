#![feature(plugin)]
#![plugin(rocket_codegen)]
#![feature(custom_derive)]

extern crate rocket;
extern crate rocket_contrib;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

use rocket::State;
use rocket_contrib::{Json};

#[derive(Debug)]
struct StateOne {
    one: u32
}

#[derive(Debug)]
struct StateTwo {
    two: u32
}

#[derive(Debug, FromForm)]
struct QueryParam {
    q: Option<String>
}

#[derive(Debug, Serialize, Deserialize)]
struct PostData {
    q: String
}

#[get("/<param>/states?<query>")]
fn states( state_data_one: State<StateOne>, state_data_two: State<StateTwo>, param: String, query: QueryParam  )
{
    let state_one : &StateOne = state_data_one.inner();
    let state_two : &StateTwo = state_data_two.inner();
    println!("state_date_one: {:?}", state_one);
    println!("state_date_two: {:?}", state_two);
    println!("url dispatch param: {:?}", param);
    println!("url query param: {:?}", query);
}

#[post("/<param>/states", format = "application/json", data = "<post_data>")]
fn post_states( state_data_one: State<StateOne>, state_data_two: State<StateTwo>, param: String, post_data: Json<PostData> )
{
    let state_one : &StateOne = state_data_one.inner();
    let state_two : &StateTwo = state_data_two.inner();
    println!("state_date_one: {:?}", state_one);
    println!("state_date_two: {:?}", state_two);
    println!("url dispatch param: {:?}", param);
    println!("url post data: {:?}", post_data);
}

fn main() {
    println!("curl http://localhost:8000/hello/states?q=query_param");
    println!("curl -H \"Content-Type: application/json\" -X POST -d '{{\"q\":\"post data string\"}}' http://localhost:8000/hello/states");
    rocket::ignite()
        .manage(StateOne { one: 1 })
        .manage(StateTwo { two: 2 })
        .mount("/", routes![states, post_states]).launch();
}

